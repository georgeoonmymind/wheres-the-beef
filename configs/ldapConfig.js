module.exports = {
  PHOTO_DIR: './public/img/userPhotos',
  OPTS: {
    server: {
      url: 'ldap://ADDSBOS01.ezesoft.net',
      bindDn: 'shuzhang@ezesoft.net',
      bindCredentials: 'Eze_SZ_100',
      searchBase: 'dc=ezesoft,dc=net',
      searchFilter: '(sAMAccountName={{username}})'
    }
  }
};
