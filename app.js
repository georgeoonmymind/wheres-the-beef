var express = require('express'),
  engine = require('ejs-locals'),
  session = require('express-session'),
  passport = require('passport'),
  bodyParser = require('body-parser'),
  LdapStrategy = require('passport-ldapauth'),
  ldapConfig = require('./configs/ldapConfig'),
  authConfig = require('./configs/authConfig');

var app = express();

var ldapStrategy = new LdapStrategy(ldapConfig.OPTS);
passport.use(ldapStrategy);

app.engine('ejs', engine);
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(express.static('public'));
app.use(session({
  secret: authConfig.SALT,
  resave: false,
  saveUninitialized: true
}));

app.use(function (req, res, next) {
  res.locals.session = req.session;
  next();
});

app.use(require('./controllers'));

var PORT = process.env.PORT || 80;
app.listen(PORT);
console.log("Server listening in: http://localhost:" + PORT);
