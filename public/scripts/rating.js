$(document).ready(function() {
	$('#understandSlider').slider({tooltip: 'hide'});
    $('#agreeSlider').slider({tooltip: 'hide'});
    $('#likeSlider').slider({tooltip: 'hide'});
});

angular.module('ratingApp',['ui.bootstrap']);
angular.module('ratingApp').controller('goalsCtrl', function ($scope, $http, $timeout) {
  $scope.loading = true;
  var lookupTable = {
    Accounting: {
      rapidViewId:    105,
      mainVersionId:  15324,
      goalVersionIds: [16699, 16700, 16737, 16697, 16695, 16698, 16696]
    },
    Analytics: {
      rapidViewId:    239,
      mainVersionId:  15294,
      goalVersionIds: [16765,16766]
    },
    Compliance: {
      rapidViewId:    254,
      mainVersionId:  16621,
      goalVersionIds: [16793,16794,16795,16796,16797]
    },
    "Data Management": {
      rapidViewId:    101,
      mainVersionId:  15793,
      goalVersionIds: [16300,16636,16734]
    },
    "Reference Data": {
      rapidViewId:    259,
      mainVersionId:  15726,
      goalVersionIds: [16629,16497,16007,16692,16758]
    },
    Reporting: {
      rapidViewId:    173,
      mainVersionId:  15897,
      goalVersionIds: [16760,16761]
    },
    "Data Services": {
      rapidViewId:    31,
      mainVersionId:  15113,
      goalVersionIds: [16782,16753,16754,16783,16784,16755,16756,16757]
    },
    Framework: {
      rapidViewId:    201,
      mainVersionId:  16297,
      goalVersionIds: [15590, 16705, 16706, 16707, 16708, 16709]
    },
    "Calculation Engine": {
      rapidViewId:    280,
      mainVersionId:  15598,
      goalVersionIds: [16770, 16771, 16772, 16773]
    },
    "Valuation": {
      rapidViewId:    22,
      mainVersionId:  16743,
      goalVersionIds: [16752, 16779, 16751, 16780]
    },
    Trading: {
      rapidViewId:    253,
      mainVersionId:  15522,
      goalVersionIds: [15662, 16710, 16711, 16712]      
    }
  };
  var teamPtr = lookupTable[teamPassToAngular];
  
  if(teamPtr != null) {
    var goalErrorData =[ { name: "Check out the Quarterly Goal Tracker!",
                          description: "Connection to Jira seems to be down at the moment...",
                          href: "http://devmetrics01/Reports/Pages/Report.aspx?ItemPath=%2fEze.Management.Reports%2fQuarterly+Goal+Tracker" } ];
    
    var jiraURI = 'http://10.28.66.101/jiraproxy/rest/greenhopper/1.0/xboard/plan/backlog/versions';
    jiraURI += '?rapidViewId='+teamPtr.rapidViewId;
    
    $http.get(jiraURI, {cache: true}).then(function(response) {
      var data        = response.data;
      if(data.versionData != null){
        var allVersions = data.versionData.versionsPerProject[data.projects[0].id];
        
        //Create a lookup table
        var allVersionsById = {};
        for(var i=0; i < allVersions.length; i++){
          var id = allVersions[i].id;
          allVersionsById[id] = allVersions[i];
        }
        
        //Now process the ones we care about
        var gData = [];
        for(var i=0; i < teamPtr.goalVersionIds.length; i++){
          gData[i] = allVersionsById[teamPtr.goalVersionIds[i]];
          gData[i]["href"]  = "https://jira.ezesoft.net/secure/RapidBoard.jspa"
          gData[i]["href"] += "?rapidView="+teamPtr.rapidViewId;
          gData[i]["href"] += "&view=reporting&chart=versionReport&version="+teamPtr.goalVersionIds[i];
        }
        $scope.goalData = gData;
      } else {
        $scope.goalData = goalErrorData;
      }
      $scope.loading = false;
    }, function(response){
      $scope.goalData = goalErrorData;
      $scope.loading  = false;
    });
  } else {
    $scope.goalData = [ { name: teamPassToAngular+" has no quarterly goals defined in Jira",
                          description: '"If you don\'t know where you are going, you\'ll end up someplace else." -Yogi Berra',
                          href: "http://www.scaledagileframework.com/team-pi-objectives/" } ];
    $scope.loading = false;
  }
});
