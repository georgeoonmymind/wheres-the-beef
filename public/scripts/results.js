angular.module('beefApp', ['ui.bootstrap', 'nvd3']);
angular.module('beefApp').controller('AccordionTeams', function ($scope, $http, $timeout) {
    $scope.loading = true;
    // set up auth=--
    $http.defaults.headers.common.authtoken = authtoken;
    $http.defaults.headers.common.username = username;

    $http.get('/results/getResultData').success(function (data) {
        //console.log(data);
        var sortedData = data.sort(function (a, b) {
            if (parseInt(a.rank) > parseInt(b.rank)) {
                return 1;
            }
            if (parseInt(a.rank) < parseInt(b.rank)) {
                return -1;
            }
            return 0;
        });

        // Add a few client side fields
        for (var i = 0; i < sortedData.length; i++) {
            sortedData[i].isOpen = false;
            sortedData[i].meatText = getMeat(sortedData[i].ConfidenceIndex);
            sortedData[i].rankText = getOrdinal(sortedData[i].rank);
        }

        $scope.teams = sortedData;

        $timeout(function () {
            $scope.loading = false;
        }, 200);
    });

    $scope.confidenceOptions = {
        chart: {
            type: 'lineChart',
            height: 200,
            width: 260,
            x: function (d) {
                return new Date(d.x);
            },
            y: function (d) {
                return d.y;
            },
            xAxis: {
                tickFormat: function (d) {
                    return d3.time.format('%m/%d')(new Date(d));
                }
            },
            yAxis: {
                axisLabel: 'Confidence',
                tickFormat: function (d) {
                    return d3.format('.01f')(d);
                }
            },
            yDomain: [0.0, 10.0],
            showLegend: false,
            tooltips: false,
            useInteractiveGuideline: false,
            transitionDuration: 250
        }
    };


    $scope.rateOptions = {
        chart: {
            type: 'lineChart',
            height: 200,
            width: 260,
            x: function (d) {
                return new Date(d.x);
            },
            y: function (d) {
                return d.y;
            },
            xAxis: {
                tickFormat: function (d) {
                    return d3.time.format('%m/%d')(new Date(d));
                }
            },
            yAxis: {
                axisLabel: 'Response',
                tickFormat: function (d) {
                    return d3.format('d')(d);
                }
            },
            yDomain: [0, 50],
            showLegend: false,
            tooltips: false,
            useInteractiveGuideline: false,
            transitionDuration: 250
        }
    };

    // Hardcoded to request 12 weeks of data
    $http.get('/history/responserate/12').success(function (data) {
        var teamRates = {};
        //console.log(data);
        for (var i = 0; i < data.length; i++) {
            if (!(data[i].TeamName in teamRates)) {
                teamRates[data[i].TeamName] = [];
                teamRates[data[i].TeamName].push({
                    values: []
                });

            }
            teamRates[data[i].TeamName][0].values.push({
                x: data[i].Week,
                y: parseInt(data[i].ResponseCount)
            });
        }

        //console.log(teamRates);
        $scope.teamRateData = teamRates;
    });

    // Hardcoded to request 12 weeks of data
    $http.get('/history/confidence/12').success(function (data) {
        var teamConfidence = {};
        //console.log(data);
        for (var i = 0; i < data.length; i++) {
            if (typeof (teamConfidence[data[i].TeamName]) == 'undefined') {
                teamConfidence[data[i].TeamName] = [];
                teamConfidence[data[i].TeamName].push({
                    values: []
                });
            }
            teamConfidence[data[i].TeamName][0].values.push({
                x: data[i].Week,
                y: parseFloat(data[i].ConfidenceIndex)
            });
        }

        //console.log(teamConfidence);
        $scope.teamConfidenceData = teamConfidence;
    });
});

function getMeat(n) {
    if (n >= 9) {
        return "Can't beat the Kobe Tenderloin!"
    };
    if (n >= 8) {
        return "mmmm, Wagyu Porterhouse"
    };
    if (n >= 7) {
        return "Grassfed, Organic Filet Mignon"
    };
    if (n >= 6) {
        return "Rib Eye.  Love the Gristle."
    };
    if (n >= 5) {
        return "Good old American T-Bone"
    };
    if (n >= 4) {
        return "Prime Rib"
    };
    if (n >= 3) {
        return "McRib"
    };
    if (n >= 2) {
        return "Fenway Franks"
    };
    if (n >= 1) {
        return "Metro Mystery Meat"
    };
    return "Taco Bell's finest..."
}

function getOrdinal(n) {
    var s = ["th", "st", "nd", "rd"],
        v = n % 100;
    return n + (s[(v - 20) % 10] || s[v] || s[0]);
}

angular.module('beefApp').controller('historicalChartCtrl', function ($scope, $http) {

    $scope.confidenceOptions = {
        chart: {
            type: 'sparklinePlus',
            height: 150,
            x: function (d, i) {
                return i;
            },
            xTickFormat: function (d) {
                //return d3.time.format('%x')(new Date($scope.data[d].x))
            },
            transitionDuration: 250
        }
    };
    $scope.rateOptions = $scope.confidenceOptions;

    $scope.teamRateData = sine();
    $scope.confidenceData = volatileChart(130.0, 0.02);
    //$scope.data = volatileChart(25.0, 0.09,30);

    /* Random Data Generator (took from nvd3.org) */
    function sine() {
        var sin = [];
        var now = +new Date();

        for (var i = 0; i < 100; i++) {
            sin.push({
                x: now + i * 1000 * 60 * 60 * 24,
                y: Math.sin(i / 10)
            });
        }

        return sin;
    }

    function volatileChart(startPrice, volatility, numPoints) {
        var rval = [];
        var now = +new Date();
        numPoints = numPoints || 100;
        for (var i = 1; i < numPoints; i++) {

            rval.push({
                x: now + i * 1000 * 60 * 60 * 24,
                y: startPrice
            });
            var rnd = Math.random();
            var changePct = 2 * volatility * rnd;
            if (changePct > volatility) {
                changePct -= (2 * volatility);
            }
            startPrice = startPrice + startPrice * changePct;
        }
        return rval;
    }

})
