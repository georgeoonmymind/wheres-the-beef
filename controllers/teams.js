var express = require('express'),
  router = express.Router(),
  postgresHelper = require('../helpers/postgresHelper'),
  authHelper = require('../helpers/authHelper');
  

router.get('/', authHelper.isAuthenticated, function (req, res) {
  postgresHelper.query('select * from "Team"', function (err, results) {
    if (err) {
      console.error(err);
      res.status(500);
      return;
    }
    res.render('teamlist', {
      title: 'Teams',
      results: results
    });
  });



});

module.exports = router