var express = require('express'),
	router = express.Router(),
	passport = require('passport'),
	async = require('async'),
	ldapPhotoHelper = require('../helpers/ldapPhotoHelper'),
	authHelper = require('../helpers/authHelper');

router.post('/login', function (req, res, next) {
	console.log('loging in');
	passport.authenticate('ldapauth', { session: true }, function (err, user, info) {
		if (err) {
			return next(err); // will generate a 500 error
		}

		var is_ajax_request = false;
		if (req.xhr || req.headers.accept.indexOf('json') > -1) {
			is_ajax_request = true;
		}
		
		// Generate a JSON response reflecting authentication status
		if (!user) {
			if (is_ajax_request) {
				return res.send({ success: false, message: 'authentication failed' });
			} else {
                var errMsg= encodeURIComponent('Wrong credentials, please check and try again.');
				return res.redirect('/?errMsg='+errMsg);
			}
		}

		async.parallel([
			function (cb) {
				ldapPhotoHelper.getUserPhoto(user.sAMAccountName, req.body.password, cb);
			},
			function (cb) {
				cb(null, authHelper.getToken(user));
			}
		], function (err, results) {
			if (err) {
				res.status(500);
				if (is_ajax_request) {
					return res.send({ success: false, message: 'unknown issues' });
				} else {
					return res.redirect('/', {
						errorMsg: 'unknown issues'
					});
				}
			}

			if (is_ajax_request) {
				return res.send({ success: true, message: 'authentication succeeded', photoPath: results[0].replace('./public', ''), token: results[1] });
			} else {
				req.session.user = user;
                req.session.authtoken =  results[1];
				return res.redirect('/welcome');
			}
		});
	})(req, res, next);
});

router.all('/logout', function (req, res, next) {
	req.session.destroy();
	res.redirect('/');
	next();
});

module.exports = router;
