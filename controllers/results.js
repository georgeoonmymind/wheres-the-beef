var express = require('express'),
    router = express.Router(),
    async = require('async'),
    postgresHelper = require('../helpers/postgresHelper'),
    authHelper = require('../helpers/authHelper');

router.get('/', authHelper.isAuthenticated, function (req, res) {
    res.render('results', {
        title: 'Results Page'
    });
});

router.get('/getResultData', authHelper.isAuthenticated, function (req, res) {
    var confidenceIndexQuery = 'select ConfidenceIndices.*,rank() over (order by "ConfidenceIndex" desc) as rank from (select round(sum("Value")/count("Value")::numeric,2) as "ConfidenceIndex", (select round(sum("Value")/count("Value")::numeric,2) as "ConfidenceIndexUX" from "Response" a where "QuestionId" = 1 and a."TeamName" = b."TeamName" group by "TeamName", "QuestionId"), (select round(sum("Value")/count("Value")::numeric,2) as "ConfidenceIndexDirection" from "Response" c where "QuestionId" = 2 and c."TeamName" = b."TeamName" group by "TeamName", "QuestionId"), (select round(sum("Value")/count("Value")::numeric,2) as "ConfidenceIndexHappiness" from "Response" d where "QuestionId" = 3 and d."TeamName" = b."TeamName" group by "TeamName", "QuestionId"), "TeamName" from "Response" b group by "TeamName") as ConfidenceIndices';

    var feedbackQuery = 'SELECT "FeedbackId", "FeedbackValue", "TeamName", "UserName","UserDisplayName", "Department" FROM "Feedback" WHERE "TeamName" = \'';
    
    var mostrecentQuery = 'select max("ResponseDate"), "TeamName","Department" ,' +
' (select "Value" from "Response" b where "QuestionId"=1 and max(a."ResponseDate") = b."ResponseDate") as "ConfidenceIndexUX" ,' +
' (select "Value" from "Response" b where "QuestionId"=2 and max(a."ResponseDate") = b."ResponseDate") as "ConfidenceIndexDirection" ,'+
' (select "Value" from "Response" b where "QuestionId"=3 and max(a."ResponseDate") = b."ResponseDate") as "ConfidenceIndexHappiness"' + 
' from "Response" a'+ 
' WHERE "TeamName" = \'';

    var notseenRecentlyQuery = 'select max("ResponseDate"), "TeamName","Department" ,' +
' (select "Value" from "Response" b where "QuestionId"=1 and max(a."ResponseDate") = b."ResponseDate") as "ConfidenceIndexUX" ,' +
' (select "Value" from "Response" b where "QuestionId"=2 and max(a."ResponseDate") = b."ResponseDate") as "ConfidenceIndexDirection" ,' +
' (select "Value" from "Response" b where "QuestionId"=3 and max(a."ResponseDate") = b."ResponseDate") as "ConfidenceIndexHappiness"' +
' from "Response" a ' +
' WHERE "TeamName" = \''; 

     var mostHappyQuery = 'select max("ResponseDate"), "TeamName","Department" ,' +
' (select "Value" from "Response" b where "QuestionId"=1 and max(a."ResponseDate") = b."ResponseDate") as "ConfidenceIndexUX" ,' +
' (select "Value" from "Response" b where "QuestionId"=2 and max(a."ResponseDate") = b."ResponseDate") as "ConfidenceIndexDirection" ,' +
' (select "Value" from "Response" b where "QuestionId"=3 and max(a."ResponseDate") = b."ResponseDate") as "ConfidenceIndexHappiness"' + 
' from "Response" a' + 
' WHERE "TeamName" = \'';

    var leastHappyQuery = 'select max("ResponseDate"), "TeamName","Department" , ' +
' (select "Value" from "Response" b where "QuestionId"=1 and max(a."ResponseDate") = b."ResponseDate") as "ConfidenceIndexUX" ,' +
' (select "Value" from "Response" b where "QuestionId"=2 and max(a."ResponseDate") = b."ResponseDate") as "ConfidenceIndexDirection" ,' +
' (select "Value" from "Response" b where "QuestionId"=3 and max(a."ResponseDate") = b."ResponseDate") as "ConfidenceIndexHappiness"' + 
' from "Response" a' + 
' WHERE "TeamName" = \'';

    async.waterfall([
    function (callback) {
                postgresHelper.query(confidenceIndexQuery, function (err, results) {
                    if (err) {
                        console.error(err);
                        callback(err);
                        return;
                    }

                    callback(null, results.rows);
                });

    },
    function (results, callback) {
                async.forEachOf(results, function (value, key, callback) {                   
                    var feedbackQueryForATeam = feedbackQuery + value.TeamName + '\'' + ' ORDER BY "FeedbackId" DESC LIMIT 5';
                    postgresHelper.query(feedbackQueryForATeam, function (err, feedbacks) {
                        if (err) {
                            console.error(err);
                            return callback(err);;
                        }

                        results[key].feedbacks = feedbacks.rows || [];
                        callback();
                    });
                }, function (err) {
                    callback(err, results);
                });

    },    
    function (results, callback) {               
                async.forEachOf(results, function (value, key, callback) {                      
                    var mostrecentQueryForATeam = mostrecentQuery + value.TeamName + '\'' + 'group by "TeamName","Department" order by 2,1 desc limit 3';                   
                    postgresHelper.query(mostrecentQueryForATeam, function (err, mostrecents) {
                        if (err) {
                            console.error(err);
                            return callback(err);;
                        }

                        results[key].mostrecents = mostrecents.rows || [];
                        callback();
                    });
                }, function (err) {
                    callback(err, results);
                });

    },
    function (results, callback) {          
                async.forEachOf(results, function (value, key, callback) {                                          
                    var notSeenRecentlyQueryForATeam = notseenRecentlyQuery + value.TeamName + '\'' + ' group by "TeamName","Department" having max("ResponseDate") < current_date-30 order by 2,1 asc limit 3';                     
                    postgresHelper.query(notSeenRecentlyQueryForATeam, function (err, notseenrecently) {
                        if (err) {
                            console.error(err);
                            return callback(err);;
                        }

                        results[key].notseenrecently = notseenrecently.rows || [];
                        callback();
                    });
                }, function (err) {
                    callback(err, results);
                });

    },
    function (results, callback) {          
                async.forEachOf(results, function (value, key, callback) {                                          
                    var mostHappyQueryForATeam = mostHappyQuery + value.TeamName + '\'' + ' group by "TeamName","Department" order by 2,6 desc limit 3 ';                     
                    postgresHelper.query(mostHappyQueryForATeam, function (err, mostHappy) {
                        if (err) {
                            console.error(err);
                            return callback(err);;
                        }

                        results[key].mostHappy = mostHappy.rows || [];
                        callback();
                    });
                }, function (err) {
                    callback(err, results);
                });

    },
    function (results, callback) {          
                async.forEachOf(results, function (value, key, callback) {                                          
                    var leastHappyQueryForATeam = leastHappyQuery + value.TeamName + '\'' + ' group by "TeamName","Department" order by 2,6 asc limit 3';                     
                    postgresHelper.query(leastHappyQueryForATeam, function (err, leastHappy) {
                        if (err) {
                            console.error(err);
                            return callback(err);;
                        }

                        results[key].leastHappy = leastHappy.rows || [];
                        callback();
                    });
                }, function (err) {
                    callback(err, results);
                });

    }    
],
        // optional callback
        function (err, result) {
            if (err) {
                res.status(500);
                return res.send({
                    errorMsg: 'Internal Error'
                });
            }
            res.send(result);
        });


});

module.exports = router
