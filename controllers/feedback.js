var express = require('express')
  , router = express.Router(),
  postgresHelper = require('../helpers/postgresHelper'),
  authHelper = require('../helpers/authHelper');

var bodyParser = require('body-parser')
router.use(bodyParser.json());       // to support JSON-encoded bodies
router.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));


router.get('/:team', authHelper.isAuthenticated, function (req, res) {
  var team = req.params.team;
  var success = req.query.success;
  res.render('feedback', {
    title: 'Give feedback for ' + team + ' team!',
    team: team,
    success: success
  });
});


router.post('/', function (req, res) {
  var feedbackText = req.body.feedbacktext,
      teamName = req.body.selectedTeamName;
 
  var user = req.session.user;
  
  var insertQuery = ' INSERT INTO "Feedback"("FeedbackValue", "TeamName", "UserName", "Department", "UserDisplayName") VALUES (\'' 
                      + feedbackText  +'\',\'' + teamName +'\',\'' + user.sAMAccountName + '\',\'' + user.department + '\',\'' + user.displayName + '\');' 
  
  postgresHelper.query(insertQuery, function (err, results) {
    if (err) {
      console.error(err);
      res.status(500);
      return;      
    }
    res.redirect('/feedback/' +  teamName +  '?success=true');    
  });  
});

module.exports = router