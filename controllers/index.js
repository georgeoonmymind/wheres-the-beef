var express = require('express'),
    router = express.Router(),
    auth = require('../helpers/authHelper');

// register controllers
router.use('/', require('./auth'));
router.use('/users', require('./users'));
router.use('/metrics', require('./metrics'));
router.use('/', require('./vote'));
router.use('/ratings', require('./ratings'));
router.use('/results', require('./results'));
router.use('/teams', require('./teams'));
router.use('/feedback', require('./feedback'));
router.use('/history',require('./history'));

router.get('/', function (req, res) {
    var model = {
        title: 'Where\'s the Beef?'
    };

    if (req.query.errMsg) {
        model.errMsg = req.query.errMsg;
    }
    res.render('index', model);
});

router.get('/welcome', auth.isAuthenticated, function (req, res) {
    res.render('welcome', {
        title: 'Welcome'
    });
});

router.get('/401', function (req, res) {
    res.render('401', {
        title: 'Please log in to continue'
    });
});


module.exports = router;
