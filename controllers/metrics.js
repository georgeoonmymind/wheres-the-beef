var express = require('express'),
  router = express.Router(),
  auth = require('../helpers/authHelper');

// Define routes handling profile requests
router.get('/', auth.isAuthenticated, function (req, res) {
  res.send({
    storiesFinished: 11
  });
});

module.exports = router