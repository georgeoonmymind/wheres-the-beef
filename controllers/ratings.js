var express = require('express'),
    router = express.Router(),
    postgresHelper = require('../helpers/postgresHelper'),
    authHelper = require('../helpers/authHelper');

var bodyParser = require('body-parser')
router.use(bodyParser.json()); // to support JSON-encoded bodies
router.use(bodyParser.urlencoded({ // to support URL-encoded bodies
    extended: true
}));

router.get('/:team', authHelper.isAuthenticated, function (req, res) {
    var team = req.params.team;

    res.render('rating', {
        title: 'Rate the ' + team + ' team!',
        team: team
    });
});

router.post('/', authHelper.isAuthenticated, function (req, res) {
    var understandRating = req.body.understandSlider,
        agreeRating = req.body.agreeSlider,
        likeRating = req.body.likeSlider,
        teamName = req.body.teamName;

    var user = req.session.user;
    var date = (new Date().toLocaleString());

    var insertQuery = ' INSERT INTO "Response"("QuestionId", "TeamId", "Value", "UserName", "Department","ResponseDate", "TeamName") VALUES (1,1,' + understandRating + ',\'' + user.sAMAccountName + '\',\'' + user.department + '\',\'' + date + '\',\'' + teamName + '\');'

    insertQuery = insertQuery + '\n ' + ' INSERT INTO "Response"("QuestionId", "TeamId", "Value", "UserName", "Department","ResponseDate", "TeamName") VALUES (2,1,' + agreeRating + ',\'' + user.sAMAccountName + '\',\'' + user.department + '\',\'' + date + '\',\'' + teamName + '\');'

    insertQuery = insertQuery + '\n ' + ' INSERT INTO "Response"("QuestionId", "TeamId", "Value", "UserName", "Department","ResponseDate", "TeamName") VALUES (3,1,' + likeRating + ',\'' + user.sAMAccountName + '\',\'' + user.department + '\',\'' + date + '\',\'' + teamName + '\');'

    postgresHelper.query(insertQuery, function (err, results) {
        if (err) {
            console.error(err);
            res.status(500);
            return;
        }
        res.redirect('/feedback/' + teamName);
    });
});

module.exports = router
