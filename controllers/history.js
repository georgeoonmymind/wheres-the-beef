var express = require('express'),
  router = express.Router(),
  postgresHelper = require('../helpers/postgresHelper'),
  auth = require('../helpers/authHelper');

// Define routes handling profile requests
router.get('/confidence/:weeks', auth.isAuthenticated, function (req, res) {
  var weeks = req.params.weeks;
  
  var queryString = 'SELECT max("TeamName") AS "TeamName", date_trunc(\'week\', "ResponseDate") AS "Week", avg("ConfidenceIndex") AS "ConfidenceIndex" FROM "HistoricalConfidenceIndexData" WHERE "ResponseDate" > now() - interval \'' + weeks + ' weeks\' GROUP BY "Week", "TeamName" ORDER BY "Week"';
  
  postgresHelper.query(queryString, function (err, results) {
    if (err) {
      console.error(err);
      res.status(500);
      return;
    }
    res.writeHead(200, {
      'Content-Type' : 'x-application/json'
    });
    res.end(JSON.stringify(results.rows));
  });
});

router.get('/responserate/:weeks', auth.isAuthenticated, function (req, res) {
    var weeks = req.params.weeks;
  
  var queryString = 'SELECT max("TeamName") AS "TeamName", date_trunc(\'week\', "ResponseDate") AS "Week", sum("ResponseCount") AS "ResponseCount" FROM "HistoricalResponseCountData" WHERE "ResponseDate" > now() - interval \'' + weeks + ' weeks\' GROUP BY "Week", "TeamName" ORDER BY "Week"';
  
  postgresHelper.query(queryString, function (err, results) {
    if (err) {
      console.error(err);
      res.status(500);
      return;
    }
    res.writeHead(200, {
      'Content-Type' : 'x-application/json'
    });
    res.end(JSON.stringify(results.rows));
  });
});

module.exports = router