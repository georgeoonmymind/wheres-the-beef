var express = require('express')
  , router = express.Router()

router.get('/vote', function(req, res) {
  res.render('vote', {
    title: 'The VOTE page!'
  });
});

module.exports = router