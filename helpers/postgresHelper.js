var pg = require('pg');

var conString = process.env.DATABASE_URL || "postgres://postgres:postgres@nuthi990/wheres-the-beef";


var query = function (queryConfig, cb) {
  pg.connect(conString, function (err, client, done) {
    if (err) {
      return console.error('error fetching client from pool', err);
    }
    client.query(queryConfig, function (err, result) {
      done();
      cb(err, result);
    });

  });

}    


exports.query = query;
