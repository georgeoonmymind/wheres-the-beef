var ldap = require('ldapjs'),
    fs = require('fs'),
    ldapConfig = require('../configs/ldapConfig'),
    miscConfig = require('../configs/miscConfig');




// get all attribute as strings except for 'thumbnailPhoto' as buffer
function getProperObject(entry) {
    var obj = {
        dn: entry.dn.toString(),
        controls: []
    };
    entry.attributes.forEach(function (a) {
        var buf = a.buffers;
        var val = a.vals;
        var item;
        if (a.type == 'thumbnailPhoto')
            item = buf;
        else
            item = val;
        if (item && item.length) {
            if (item.length > 1) {
                obj[a.type] = item.slice();
            } else {
                obj[a.type] = item[0];
            }
        } else {
            obj[a.type] = [];
        }
    });
    entry.controls.forEach(function (element, index, array) {
        obj.controls.push(element.json);
    });
    return obj;
}

exports.getUserPhoto = function (sAMAccountName, password, cb) {
    var opts = {
        filter: '(sAMAccountName=' + sAMAccountName + ')',
        scope: 'sub'
    };

    var client = ldap.createClient({
        url: 'ldap://ADDSBOS01.ezesoft.net'
    });

    client.bind(sAMAccountName + '@ezesoft.net', password, function (err) {
        if (err) {
            console.log("errror with authentication when trying to get user photo : ");
            console.error(err);
            return;
        }

        client.search('dc=ezesoft,dc=net', opts, function (err, res) {
            if (err) {
                console.error(err);
                return;
            }

            res.on('searchEntry', function (entry) {
                var obj = getProperObject(entry);

                if (!fs.existsSync(miscConfig.PHOTO_DIR)) {
                    fs.mkdirSync(miscConfig.PHOTO_DIR);
                }

                var photoPath = miscConfig.PHOTO_DIR + '/' + sAMAccountName + '.jpg';

                fs.writeFile(photoPath, obj.thumbnailPhoto, function (err) {
                    if (err) {
                        console.log("errror writing thumbnail: " + err);
                        cb(err, null);
                    } else {
                        console.log("thumbnail was saved!");
                        cb(null, photoPath);
                    }
                });
            });
        });
    });
};

exports.getUserPhotoByUsername = function (sAMAccountName, cb) {
    var opts = {
        filter: '(sAMAccountName=' + sAMAccountName + ')',
        scope: 'sub'
    };

    var client = ldap.createClient({
        url: 'ldap://ADDSBOS01.ezesoft.net'
    });

    client.bind(ldapConfig.OPTS.server.bindDn, ldapConfig.OPTS.server.bindCredentials, function (err) {
        if (err) {
            console.log("errror with authentication when trying to get user photo : ");
            console.error(err);
            return;
        }

        client.search('dc=ezesoft,dc=net', opts, function (err, res) {
            if (err) {
                console.error(err);
                return;
            }

            res.on('searchEntry', function (entry) {
                var obj = getProperObject(entry);

                if (!fs.existsSync(miscConfig.PHOTO_DIR)) {
                    fs.mkdirSync(miscConfig.PHOTO_DIR);
                }

                var photoPath = miscConfig.PHOTO_DIR + '/' + sAMAccountName + '.jpg';

                fs.writeFile(photoPath, obj.thumbnailPhoto, function (err) {
                    if (err) {
                        console.log("errror writing thumbnail: " + err);
                        cb(err, null);
                    } else {
                        console.log("thumbnail was saved!");
                        cb(null, photoPath);
                    }
                });
            });
        });
    });
};
