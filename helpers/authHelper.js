var jwt = require('jsonwebtoken'),
    SALT = require('../configs/authConfig').SALT;

// As with any middleware it is quintessential to call next()
// if the user is authenticated
exports.isAuthenticated = function (req, res, next) {
    var is_ajax_request = false;
    if (req.xhr || req.headers.accept.indexOf('json') > -1) {
        is_ajax_request = true;
    }

    if (is_ajax_request) {
        var token = req.headers.authtoken;
        var username = req.headers.username;
        if (token && username) {
            var decoded = jwt.verify(token, SALT);
            if (decoded.username === username) {
                return next();
            }
        }

        res.status(401);
        res.send({
            message: "unauthenticated"
        });
    } else if (req.session.user) {
        return next();
    } else {
        res.redirect('/401');
    }
}

exports.getToken = function (user) {
    var token = jwt.sign({
        username: user.sAMAccountName
    }, SALT);
    return token;
}
